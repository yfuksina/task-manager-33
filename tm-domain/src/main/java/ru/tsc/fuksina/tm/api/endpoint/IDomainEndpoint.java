package ru.tsc.fuksina.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IDomainEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String SPACE = "http://endpoint.tm.fuksina.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(
            @NotNull final String host, @NotNull final String port
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXmlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonJaxbLoadResponse loadDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJaxbLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJsonJaxbSaveResponse saveDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJaxbSaveRequest request
    );

    @NotNull
    @WebMethod
    DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXmlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataXmlJaxbLoadResponse loadDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJaxbLoadRequest request
    );

    @NotNull
    @WebMethod
    DataXmlJaxbSaveResponse saveDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJaxbSaveRequest request
    );

    @NotNull
    @WebMethod
    DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXmlSaveRequest request
    );

}
