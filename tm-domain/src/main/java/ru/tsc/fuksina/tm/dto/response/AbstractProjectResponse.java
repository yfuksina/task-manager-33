package ru.tsc.fuksina.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable Project project;

}
