package ru.tsc.fuksina.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.ServerAboutRequest;
import ru.tsc.fuksina.tm.dto.request.ServerVersionRequest;
import ru.tsc.fuksina.tm.dto.response.ServerAboutResponse;
import ru.tsc.fuksina.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface ISystemEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String SPACE = "http://endpoint.tm.fuksina.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(
            @NotNull final String host, @NotNull final String port
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(
            @WebParam(name = "request", partName = "request")
            @NotNull ServerAboutRequest request
    );

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(
            @WebParam(name = "request", partName = "request")
            @NotNull ServerVersionRequest request
    );

}
