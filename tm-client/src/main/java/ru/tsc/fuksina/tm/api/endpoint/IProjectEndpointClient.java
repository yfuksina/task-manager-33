package ru.tsc.fuksina.tm.api.endpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {
}
