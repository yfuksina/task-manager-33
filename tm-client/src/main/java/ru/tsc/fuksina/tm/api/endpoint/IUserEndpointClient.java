package ru.tsc.fuksina.tm.api.endpoint;

public interface IUserEndpointClient extends IEndpointClient, IUserEndpoint {
}
