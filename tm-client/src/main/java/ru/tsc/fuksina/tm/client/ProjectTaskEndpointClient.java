package ru.tsc.fuksina.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.IProjectTaskEndpointClient;
import ru.tsc.fuksina.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.fuksina.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.fuksina.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.fuksina.tm.dto.response.TaskUnbindFromProjectResponse;

public class ProjectTaskEndpointClient extends AuthEndpointClient implements IProjectTaskEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

}
