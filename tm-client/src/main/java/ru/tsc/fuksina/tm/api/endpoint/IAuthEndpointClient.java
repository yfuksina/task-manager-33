package ru.tsc.fuksina.tm.api.endpoint;

public interface IAuthEndpointClient extends IEndpointClient, IAuthEndpoint {
}
