package ru.tsc.fuksina.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.IAuthEndpointClient;
import ru.tsc.fuksina.tm.dto.request.UserLoginRequest;
import ru.tsc.fuksina.tm.dto.request.UserLogoutRequest;
import ru.tsc.fuksina.tm.dto.request.UserShowProfileRequest;
import ru.tsc.fuksina.tm.dto.response.UserLoginResponse;
import ru.tsc.fuksina.tm.dto.response.UserLogoutResponse;
import ru.tsc.fuksina.tm.dto.response.UserShowProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserShowProfileResponse profile(@NotNull final UserShowProfileRequest request) {
        return call(request, UserShowProfileResponse.class);
    }

}
