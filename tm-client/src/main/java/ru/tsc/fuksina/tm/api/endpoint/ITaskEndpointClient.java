package ru.tsc.fuksina.tm.api.endpoint;

public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {
}
