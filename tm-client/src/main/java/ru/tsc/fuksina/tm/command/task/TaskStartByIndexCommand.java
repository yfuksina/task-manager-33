package ru.tsc.fuksina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start task by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(index, Status.IN_PROGRESS));
    }

}
