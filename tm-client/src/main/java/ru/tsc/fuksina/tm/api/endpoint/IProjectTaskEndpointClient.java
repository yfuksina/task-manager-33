package ru.tsc.fuksina.tm.api.endpoint;

public interface IProjectTaskEndpointClient extends IEndpointClient, IProjectTaskEndpoint {
}
