package ru.tsc.fuksina.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.fuksina.tm.api.endpoint.*;
import ru.tsc.fuksina.tm.api.repository.ICommandRepository;
import ru.tsc.fuksina.tm.api.service.*;
import ru.tsc.fuksina.tm.client.*;
import ru.tsc.fuksina.tm.command.AbstractCommand;
import ru.tsc.fuksina.tm.command.server.ConnectCommand;
import ru.tsc.fuksina.tm.command.server.DisconnectCommand;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.fuksina.tm.exception.system.CommandNotSupportedException;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.model.User;
import ru.tsc.fuksina.tm.repository.CommandRepository;
import ru.tsc.fuksina.tm.service.*;
import ru.tsc.fuksina.tm.util.SystemUtil;
import ru.tsc.fuksina.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator{

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.fuksina.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final AuthEndpointClient authEndpointClient = new AuthEndpointClient();

    @Getter
    @NotNull
    private final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final DomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @Getter
    @NotNull
    private final ProjectTaskEndpointClient projectTaskEndpointClient = new ProjectTaskEndpointClient();

    @Getter
    @NotNull
    private final SystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @Getter
    @NotNull
    private final TaskEndpointClient endpointClient = new TaskEndpointClient();

    @Getter
    @NotNull
    private final UserEndpointClient userEndpointClient = new UserEndpointClient(authEndpointClient);

    @Getter
    @NotNull
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        fileScanner.start();
//        connect();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN**");
//        disconnect();
    }

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class< ? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final Object object = clazz.newInstance();
        final AbstractCommand command = (AbstractCommand) object;
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        runTest();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void runTest() {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();

        @NotNull final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);
        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest();
        userRegistryRequest.setLogin("new");
        userRegistryRequest.setPassword("user");
        userRegistryRequest.setEmail("email@email.com");
        @Nullable final User user = userEndpoint.registryUser(userRegistryRequest).getUser();
        System.out.println("New user login: " + user.getLogin());
        @NotNull final String userId = user.getId();

        @NotNull final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest("Project", "About project");
        projectCreateRequest.setUserId(userId);
        System.out.println(projectEndpoint.createProject(projectCreateRequest));

        @NotNull final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest("Task", "About task");
        taskCreateRequest.setUserId(userId);
        System.out.println(taskEndpoint.createTask(taskCreateRequest));

        @NotNull final ProjectListRequest projectListRequest = new ProjectListRequest();
        projectListRequest.setUserId(userId);
        System.out.println(projectEndpoint.listProject(projectListRequest).getProjects());

        @NotNull final TaskListRequest taskListRequest = new TaskListRequest();
        taskListRequest.setUserId(userId);
        System.out.println(taskEndpoint.listTask(taskListRequest).getTasks());
    }

}
