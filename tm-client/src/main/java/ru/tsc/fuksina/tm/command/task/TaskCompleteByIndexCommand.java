package ru.tsc.fuksina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-index";

    @NotNull
    public static final String DESCRIPTION = "Complete task by index";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskEndpoint().changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(index, Status.COMPLETED));
    }

}
