package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskEndpointClient getEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    IProjectTaskEndpointClient getProjectTaskEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

}
