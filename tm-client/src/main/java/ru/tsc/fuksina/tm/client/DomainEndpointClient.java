package ru.tsc.fuksina.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.IDomainEndpointClient;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.dto.response.*;

public class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NotNull final DataJsonJaxbLoadRequest request) {
        return call(request, DataJsonJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NotNull final DataJsonJaxbSaveRequest request) {
        return call(request, DataJsonJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NotNull final DataXmlJaxbLoadRequest request) {
        return call(request, DataXmlJaxbLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NotNull final DataXmlJaxbSaveRequest request) {
        return call(request, DataXmlJaxbSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull final DataYamlFasterXmlLoadRequest request) {
        return call(request, DataYamlFasterXmlLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull final DataYamlFasterXmlSaveRequest request) {
        return call(request, DataYamlFasterXmlSaveResponse.class);
    }

}
