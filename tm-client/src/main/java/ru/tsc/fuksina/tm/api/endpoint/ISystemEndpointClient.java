package ru.tsc.fuksina.tm.api.endpoint;

public interface ISystemEndpointClient extends IEndpointClient, ISystemEndpoint {
}
