package ru.tsc.fuksina.tm.client;

import lombok.SneakyThrows;
import ru.tsc.fuksina.tm.api.endpoint.ISystemEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class SystemEndpointClientSoap {

    @SneakyThrows
    public static void main(String[] args) {
        final String wsdl = "http://127.0.0.1:8080/SystemEndpoint?wsdl";
        final ISystemEndpoint endpoint = Service.create(
                new URL(wsdl),
                new QName("http://endpoint.tm.fuksina.tsc.ru/", "SystemEndpointService")
        ).getPort(ISystemEndpoint.class);
    }

}
