package ru.tsc.fuksina.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.IUserEndpointClient;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.dto.response.*;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    public UserEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
        final UserEndpointClient userEndpointClient = new UserEndpointClient(authEndpointClient);
        System.out.println(userEndpointClient.changeUserPassword(new UserChangePasswordRequest("abc")).getSuccess());
    }

}
