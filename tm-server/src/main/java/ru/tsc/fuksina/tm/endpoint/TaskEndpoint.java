package ru.tsc.fuksina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.api.service.ITaskService;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.dto.response.*;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.fuksina.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getId();
        @Nullable Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskClearRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskCreateRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String name = request.getName();
        @Nullable String description = request.getDescription();
        @Nullable Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskListRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Sort sort = request.getSort();
        @Nullable List<Task> tasks = getTaskService().findAll(sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskRemoveByIdRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getUserId();
        @Nullable Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskShowByIdRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getId();
        @Nullable Task task = getTaskService().findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIndexResponse showTaskByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskShowByIndexRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByProjectIdResponse showTaskByProjectId(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskShowByProjectIdRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String projectId = request.getProjectId();
        @Nullable List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskUpdateByIdRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getId();
        @Nullable String name = request.getName();
        @Nullable String description = request.getDescription();
        @Nullable Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable String name = request.getName();
        @Nullable String description = request.getDescription();
        @Nullable Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
