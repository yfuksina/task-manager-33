package ru.tsc.fuksina.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.*;
import ru.tsc.fuksina.tm.api.repository.IProjectRepository;
import ru.tsc.fuksina.tm.api.repository.ITaskRepository;
import ru.tsc.fuksina.tm.api.repository.IUserRepository;
import ru.tsc.fuksina.tm.api.service.*;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.endpoint.*;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.repository.ProjectRepository;
import ru.tsc.fuksina.tm.repository.TaskRepository;
import ru.tsc.fuksina.tm.repository.UserRepository;
import ru.tsc.fuksina.tm.service.*;
import ru.tsc.fuksina.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private  final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService();

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run() {
        initPID();
        initUsers();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        backup.start();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN**");
        backup.stop();
    }

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name+ "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}
