package ru.tsc.fuksina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.dto.request.*;
import ru.tsc.fuksina.tm.dto.response.*;
import ru.tsc.fuksina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.fuksina.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJsonFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJsonFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJsonJaxbLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJsonJaxbSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataXmlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataXmlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataXmlJaxbLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataXmlJaxbSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataYamlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataYamlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }

}
