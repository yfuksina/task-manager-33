package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

}
